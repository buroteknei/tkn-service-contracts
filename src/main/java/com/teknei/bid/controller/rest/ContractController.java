package com.teknei.bid.controller.rest;

import com.teknei.bid.command.*;
import com.teknei.bid.dto.ContractSignedDTO;
import com.teknei.bid.dto.SignContractRequestDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/contract")
public class ContractController {

    @Autowired
    @Qualifier("contractCommand")
    private Command contractCommand;
    @Autowired
    @Qualifier("contractCommandWithCerts")
    private Command contractCommandWithCerts;

    private static final Logger log = LoggerFactory.getLogger(ContractController.class);

    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO contractRequestDTO) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".signContract ");
        log.debug("Requesting /contrato/sign: {}", contractRequestDTO);
        try {
            CommandRequest commandRequest = new CommandRequest();
            commandRequest.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
            commandRequest.setData(contractRequestDTO.getBase64Finger());
            commandRequest.setData2(contractRequestDTO.getContentType());
            commandRequest.setId(contractRequestDTO.getOperationId());
            commandRequest.setUsername(contractRequestDTO.getUsername());
            commandRequest.setScanId(contractRequestDTO.getHash());
            CommandResponse response = contractCommand.execute(commandRequest);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contrato/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContract ");
        byte[] contract = new byte[0];
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract pre filled for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contractWithCert/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContractWithCerts(@PathVariable("id") Long id, @PathVariable("username") String username){
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts ");
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts id      :"+id);
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts username:"+username);
        byte[] contract = new byte[0];
        try{
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_PREFILLED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommandWithCerts.execute(request);
            log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus():"+response.getStatus());
            if (response.getStatus().equals(Status.CONTRACT_OK)) 
            {
            	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A TRUE");
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            }
            else 
            {
            	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A FALSE");
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contratoB64/{id}/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addPlainSignedContract ");
        byte[] contract = new byte[0];
        String contractB64 = "";
        JSONObject jsonObject = new JSONObject();
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                contractB64 = Base64Utils.encodeToString(contract);
                jsonObject.put("contract", contractB64);
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
            } else {
                jsonObject.put("contract", "");
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addPlainSignedContract ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(contractSignedDTO.getOperationId());
            request.setUsername(contractSignedDTO.getUsername());
            request.setContract(Base64Utils.decodeFromString(contractSignedDTO.getFileB64()));
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error adding signed contract with message: {}", e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Adds signed contract to the document casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> addContratoFirmado(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContratoFirmado ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(id);
            request.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED_MOBILES);
            request.setContract(file.getBytes());
            request.setUsername("NA");
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error adding signed contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
