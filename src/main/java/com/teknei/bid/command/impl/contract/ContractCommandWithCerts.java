package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.*;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class ContractCommandWithCerts implements Command {

    @Autowired
    @Qualifier(value = "persistContractCommand")
    private Command persistContractCommand;
    @Autowired
    @Qualifier(value = "storeTasContractCommand")
    private Command storeTasContractCommand;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    @Qualifier(value = "parseContractSignedCommand")
    private Command parseContractSignedCommand;
    @Autowired
    private BidClieRegEstaRepository regEstaRepository;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    @Autowired
    @Qualifier(value = "parseContractCommandWithSabadellAndCerts")
    private Command parseContractCommandWithSabadellAndCerts;
    private static final String ESTA_PROC = "CONS-CONT";
    private static final String ESTA_PROC_2 = "AUT-DAC";
    private static final String ESTA_PROC_3 = "FIR-CONT";
    @Value("${tkn.contract.type.withSerial}")
    private Boolean withSerial;
    @Value("${tkn.contract.type.withSabadellV1}")
    private Boolean withSabadellV1;
    private static final Logger log = LoggerFactory.getLogger(ContractCommandWithCerts.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute ");
        byte[] contract = null;
        CommandResponse parseResponse = null;
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute    parseContractCommandWithSabadellAndCerts  inicia  ");
        parseResponse = parseContractCommandWithSabadellAndCerts.execute(request);
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute    parseContractCommandWithSabadellAndCerts  finaliza ");
        
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute parseResponse.getStatus():"+parseResponse.getStatus());
        if (parseResponse.getStatus().equals(Status.CONTRACT_PREFILLED_ERROR)) 
        {
            parseResponse.setDesc(String.valueOf(parseResponse.getStatus().getValue()));
            parseResponse.setStatus(Status.CONTRACT_ERROR);
            saveStatus(request.getId(), Status.CONTRACT_PREFILLED_ERROR, request.getUsername());
            return parseResponse;
        }
        contract = parseResponse.getContract();
        saveStatus(request.getId(), Status.CONTRACT_PREFILLED_OK, request.getUsername());
        CommandRequest tasRequest = new CommandRequest();
        tasRequest.setId(request.getId());
        tasRequest.setRequestType(RequestType.CONTRACT_REQUEST_PREFILLED);
        tasRequest.setContract(contract);
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute Realiza peticion al TAS ---  INICIA ---");
        CommandResponse tasResponse = storeTasContractCommand.execute(tasRequest);
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute Realiza peticion al TAS ---  TERMINA ---:"+tasResponse.getStatus());
        if (tasResponse.getStatus().equals(Status.CONTRACT_TAS_PREFILLED_ERRROR)) 
        {
            saveStatus(request.getId(), tasResponse.getStatus(), request.getUsername());
            tasResponse.setDesc(String.valueOf(tasResponse.getStatus().getValue()));
            tasResponse.setStatus(Status.CONTRACT_ERROR);
            return tasResponse;
        }
        saveStatus(request.getId(), Status.CONTRACT_TAS_PREFILLED_OK, request.getUsername());
        CommandResponse response = new CommandResponse();
        response.setStatus(Status.CONTRACT_OK);
        response.setContract(contract);
        log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".execute Se realiza el envio a TAS y Termina con exito.");
        return response;
    }

    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".saveStatus   Se realiza el envio de guardar en lado en BD");
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private void updateStatus(Long idClient, String status, String username) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".updateStatus ");
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
