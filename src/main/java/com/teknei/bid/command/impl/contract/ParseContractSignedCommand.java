package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.SignContractRequestDTO;
import com.teknei.bid.service.ContractNom151Service;
import com.teknei.bid.service.ContractPSCSignService;
import com.teknei.bid.service.ContractSignService;
import com.teknei.bid.service.ContractTasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class ParseContractSignedCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseContractSignedCommand.class);
    @Autowired
    private ContractSignService contractSignService;
    @Autowired
    private ContractPSCSignService pscSignService;
    @Autowired
    private ContractNom151Service nom151Service;
    @Autowired
    private ContractTasService contractTasService;
    private Integer noRetry = 3; //TODO get this from config

    @Value("${tkn.cert.psc-sign-active}")
    private boolean pscSignActive = true;
    

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        SignContractRequestDTO signContractRequestDTO = new SignContractRequestDTO();
        signContractRequestDTO.setBase64Finger(request.getData());
        signContractRequestDTO.setContentType(request.getData2());
        signContractRequestDTO.setOperationId(request.getId());
        signContractRequestDTO.setHash(request.getScanId());
        try {
            byte[] contractSigned = contractSignService.signContract(signContractRequestDTO);
            if (contractSigned == null) {
                for (int i = 0; i < noRetry; i++) {
                    contractSigned = contractSignService.signContract(signContractRequestDTO);
                    if (contractSigned != null) {
                        break;
                    }
                }
            }
            if (contractSigned == null) {
                response.setStatus(Status.CONTRACT_ERROR);
            } else {
                response.setStatus(Status.CONTRACT_OK);
            }

            if (pscSignActive) {
              log.info("Calling PSC");
              byte[] signed = pscSignService.signContract(signContractRequestDTO.getOperationId(), signContractRequestDTO.getHash(), contractSigned);
              if (signed != null) {
                  contractSigned = signed;
                  log.info("Contract signed OK!!!");
              } else {
                  log.info("No contract signed");
              }
              log.info("Calling NOM151");
              byte[] nom151 = nom151Service.signContract(signContractRequestDTO.getOperationId(), contractSigned);
              if (nom151 != null) {
                  log.info("NOM151 OK");
                  try {
                      contractTasService.addContactNom151EvidencetToTas(nom151, signContractRequestDTO.getOperationId());
                      log.info("Added NOM151 to TAS services");
                  } catch (Exception e) {
                      log.error("Error adding NOM151 TAS service");
                  }
              } else {
                  log.info("No NOM151, null");
              }            	
            }
            
            response.setSignedContract(contractSigned);
        } catch (Exception e) {
            log.error("Error signing contract for request: {} with message: {}", request.getId(), e.getMessage());
            response.setStatus(Status.CONTRACT_OK);
        }
        return response;
    }


}
