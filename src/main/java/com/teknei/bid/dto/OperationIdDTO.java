package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OperationIdDTO implements Serializable {

    private Long operationId;

}