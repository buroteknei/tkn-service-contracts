package com.teknei.bid.service;

import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.persistence.entities.BidClieCert;
import com.teknei.bid.persistence.repository.BidClieCertRepository;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.jolokia.util.Base64Util;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ContractPSCSignService {

    @Value("${tkn.contract.signed.javaFullPath}")
    private String fullJavaLocation;
    @Value("${tkn.cert.absPath}")
    private String tempAbs;
    @Value("${tkn.cert.scriptStep02}")
    private String jarName;
    @Value("${tkn.cert.wsdlStep02}")
    private String wsdlStep02;
    @Value("${tkn.cert.step02User}")
    private String pscUser;
    @Value("${tkn.cert.step02Password}")
    private String pscPassword;
    @Value("${tkn.contract.signed.tempPath}")
    private String tempFolder;
    private String bankSerial;
    private String bankUsername;
    private String bankPassword;
    @Value("${tkn.cert.secret.signDoc}")
    private String secretName02;
    @Autowired
    private Decrypt decrypt;

    @PostConstruct
    private void init() {
        initPasswords02();
    }

    private void initPasswords02() 
    {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".initPasswords02 ");
        final String secretUri = "/run/secrets/" + secretName02;
        String user02 = pscUser;
        String password02 = pscPassword;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                user02 = jsonObject.optString("user", pscUser);
                password02 = jsonObject.optString("password", pscPassword);
                bankSerial = jsonObject.optString("bserial", "");
                bankUsername = jsonObject.optString("buser", "");
                bankPassword = jsonObject.optString("bpassword", "");
                user02 = decrypt(user02);
                password02 = decrypt(password02);
                bankSerial = decrypt(bankSerial);
                bankUsername = decrypt(bankUsername);
                bankPassword = decrypt(bankPassword);
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
        }
        pscUser = user02;
        pscPassword = password02;
    }


    @Autowired
    private BidClieCertRepository clieCertRepository;
    private static final String suffix = "_signed_client.pdf";
    private static final Logger log = LoggerFactory.getLogger(ContractPSCSignService.class);

    public byte[] signContract(Long idCustomer, String hash, byte[] contract) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".signContract ");
        byte[] contractsigned = null;
        List<BidClieCert> certList = clieCertRepository.findAllByIdClieAndIdEsta(idCustomer, 1);
        if (certList == null || certList.isEmpty()) {
            log.warn("No certificate found for user: {} sign only with bank", idCustomer);
        } else {
            BidClieCert cert = certList.get(0);
            String serial = cert.getSeriCert();
            String username = cert.getNomUsua();
            for (int i = 0; i<5; i++) {
                contractsigned = signDocument(contract, username, hash, serial, username, idCustomer);
                if(contractsigned != null){
                    log.info("SIGNED WITH CUSTOMER DETAILS WITH SUCCES!!!!!");
                    break;
                }
            }
        }
        log.info("Sign with bank details");
        if (contractsigned == null) {
            log.info("No sign for customer, only bank. TODO. Increment for <i>");
            contractsigned = contract;
        }
        byte[] contractSignedBank = null;
        for (int i = 0; i<5; i++) {
            contractSignedBank = signDocument(contractsigned, bankUsername, bankPassword, bankSerial, bankUsername + "_" + idCustomer, idCustomer);
            if(contractSignedBank != null){
                log.info("SIGNED WITH BANK DETAILS WITH SUCCES!!!!!");
                break;
            }
        }
        if (contractSignedBank != null) {
            contractsigned = contractSignedBank;
        }
        return contractsigned;
    }

    public byte[] testSignDocument(byte[] source, String username, String password, String serial, String tempIdentificator, Long id){
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".testSignDocument ");
        return signDocument(source, username, password, serial, tempIdentificator, id);
    }

    private byte[] signDocument(byte[] source, String username, String password, String serial, String tempIdentificator, Long id) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".signDocument ");
        String finalUriSignedTemp = new StringBuilder(tempFolder).append("/").append(tempIdentificator).append(".pdf").toString();
        byte[] document = null;
        try {
            Files.write(Paths.get(finalUriSignedTemp), source);
            log.info("Written file: {}", finalUriSignedTemp);
        } catch (IOException e) {
            log.error("Error to temporary write signed with fingers document for customer: {} with error: {}", id, e.getMessage());
            log.error("Trace: ", e);
            return null;
        }
        StringBuilder sb = new StringBuilder(fullJavaLocation).append(" -Dtrust_all_cert=true -jar ")
                .append(tempAbs).append("/").append(jarName).append(" ")
                .append(wsdlStep02).append(" ")
                .append(pscUser).append(" ")
                .append(pscPassword).append(" ")
                .append(username).append(" ")
                .append(password).append(" ")
                .append(serial).append(" ")
                .append(finalUriSignedTemp);
        log.info("[tkn-service-contracts] ::::  Invoking: {}", sb.toString());
        String fu = finalUriSignedTemp.substring(0, finalUriSignedTemp.length() - 4);
        String finalUri = new StringBuilder(fu).append(suffix).toString();
        try {
            CommandLine cmdLine = CommandLine.parse(sb.toString());
            log.info("Debug de comand line: {}", cmdLine.toString());
            DefaultExecutor executor = new DefaultExecutor();
            int exitValue = executor.execute(cmdLine);
            log.info("Exit value: {}", exitValue);
            document = Files.readAllBytes(Paths.get(finalUri));
        } catch (Exception e) {
            log.error("Error signing with customer data with message: {}", e.getMessage());
        }
        try {
            // /home/contracts/sign/Usernom151_14.pdf
            log.info("[tkn-service-contracts] :::::   Saving for NOM151 process at :{}", " /home/contracts/sign/"+id + ".pdf");
            Files.write(Paths.get(" /home/contracts/sign/Usernom151_"+id + ".pdf"), document);
            Files.delete(Paths.get(finalUri));
            Files.delete(Paths.get(finalUriSignedTemp));
        } catch (Exception e) {
            log.error("Unable to delete signed pdf with error: {}", e.getMessage());
        }
        return document;
    }

    private String decrypt(String source) {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".decrypt ");
        try {
            String decrypted = decrypt.decrypt(source);
            if (decrypted == null) {
                return source;
            }
            return new String(Base64Util.decode(decrypted));
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }

}
