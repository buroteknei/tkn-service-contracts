package com.teknei.bid.service;

import net.coobird.thumbnailator.Thumbnails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class ThumbService {

    @Value("${tkn.contract.thumb.width}")
    private Integer thumbWith;
    @Value("${tkn.contract.thumb.height}")
    private Integer thumbHeight;
    private static final Logger log = LoggerFactory.getLogger(ThumbService.class);

    public void generateThumb(String imagePathSource, String imagePathTarget) throws IOException {
        try{
            Thumbnails.of(imagePathSource)
                    .size(thumbWith, thumbHeight)
                    .outputFormat("jpeg")
                    .toFile(imagePathTarget);
        }catch (Exception e){
            log.error("Error making thumb for image, using original instead, message: {}", e.getMessage());
            Files.copy(Paths.get(imagePathSource), Paths.get(imagePathTarget));
        }
    }

}