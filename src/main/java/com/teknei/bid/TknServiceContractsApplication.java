package com.teknei.bid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
public class TknServiceContractsApplication {
	private static final Logger log = LoggerFactory.getLogger(TknServiceContractsApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(TknServiceContractsApplication.class, args);
    }

    @Bean
    public Docket api() {
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".api");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.teknei.bid.controller.rest"))
                .paths(PathSelectors.any())
                .build();
    }

}
