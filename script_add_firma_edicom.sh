#!/bin/sh
echo -n | openssl s_client -connect ecsweb.sedeb2b.com:9125/EdicomCryptoServer/services/firma?wsdl | \
   sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/examplecert03.crt
openssl x509 -in /tmp/examplecert03.crt -text
keytool -import -trustcacerts -keystore /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts \
   -storepass changeit -noprompt -alias edicomcert03 -file /tmp/examplecert03.crt