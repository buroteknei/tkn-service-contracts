#!/usr/bin/python
import zipfile
import sys
import string
import shutil
import os
from subprocess import call

"""
argv1 = full or relative path of the odt template file
argv2 = full or relative path of the temporary folder for extracting content, must end in /
argv3 = operation id
argv4 = full or relative location of the odt destination file
argv5 = name
argv6 = surname
argv7 = surname last
argv8 = birthdate with underscore separation if any
argv9 = gender in cappital letter
argv10 = birthplace
"""

""" AVB Begin
argv11 = birth country
argv12 = birth 
argv13 = rfc
argv14 = bank                     --No en formato
argv15 = account clabe destiny    --No en formato
argv16 = alias account            --No en formato
argv17 = max amount               --No en formato
argv18 = account corr             --No en formato
argv19 = clabe account            --No en formato
argv20 = address
argv21 = colony
argv22 = postal code
argv23 = city
argv24 = state
argv25 = country 
argv26 = curp
argv27 = email
argv28 = USA city                  --No en formato
argv29 = official identity
argv30 = official identity number
argv31 = mobile phone
argv32 = phone
argv33 = activity
argv34 = monthly amount 
argv35 = pepo_exp
argv36 = pare_pepo_exp
argv37 = nomb_pepo_exp
argv38 = pare
AVB End """

    #extracts content to temporal location
try:
    location = sys.argv[2] + sys.argv[3]
    zipContent = zipfile.ZipFile(sys.argv[1])
    zipContent.extractall(location)
    zipContent.close()
        #replace content for correct render
    s = open(location + '/content.xml').read()
        #get variables from cmd
    #print "Opened content from xml"
    var_BID_VAR_NOMCLIE = sys.argv[5]
    if var_BID_VAR_NOMCLIE == 'NA':
        var_BID_VAR_NOMCLIE = ''
    var_BID_VAR_NOMCLIE = var_BID_VAR_NOMCLIE.replace('_', ' ')
    #print "var_BID_VAR_NOMCLIE set"

    var_BID_VAR_APE_PAT = sys.argv[6]
    if var_BID_VAR_APE_PAT == 'NA':
        var_BID_VAR_APE_PAT = ''
    var_BID_VAR_APE_PAT = var_BID_VAR_APE_PAT.replace('_', ' ')
    #print "var_BID_VAR_APE_PAT set"


    var_BID_VAR_APE_MAT = sys.argv[7]
    if var_BID_VAR_APE_MAT == 'NA':
        var_BID_VAR_APE_MAT = ''
    var_BID_VAR_APE_MAT = var_BID_VAR_APE_MAT.replace('_',' ')
    #print "var_BID_VAR_APE_MAT set"

    var_BID_VAR_FNAC = sys.argv[8]
    if var_BID_VAR_FNAC == 'NA':
        var_BID_VAR_FNAC = ''
    var_BID_VAR_FNAC = var_BID_VAR_FNAC.replace('_',' ')
    #print "var_BID_VAR_FNAC set"

    var_BID_VAR_GENDER = sys.argv[9]

    var_BID_VAR_BIRTHPLACE = sys.argv[10]
    if var_BID_VAR_BIRTHPLACE == 'NA':
        var_BID_VAR_BIRTHPLACE = ''
    var_BID_VAR_BIRTHPLACE = var_BID_VAR_BIRTHPLACE.replace('_',' ')
    #print "var_BID_VAR_BIRTHPLACE set"
    
#AVB BEGIN    

    var_BID_VAR_BIRTHCOUNTRY = sys.argv[11]
    if var_BID_VAR_BIRTHCOUNTRY == 'NA':
        var_BID_VAR_BIRTHCOUNTRY = ''
    var_BID_VAR_BIRTHCOUNTRY = var_BID_VAR_BIRTHCOUNTRY.replace('_',' ')
    #print "var_BID_VAR_BIRTHCOUNTRY set"
    
    var_BID_VAR_BIRTH = sys.argv[12]
    if var_BID_VAR_BIRTH == 'NA':
        var_BID_VAR_BIRTH = ''
    var_BID_VAR_BIRTH = var_BID_VAR_BIRTH.replace('_',' ')
    #print "var_BID_VAR_BIRTH set"
    
    var_BID_VAR_RFC = sys.argv[13]
    if var_BID_VAR_RFC == 'NA':
        var_BID_VAR_RFC = ''
    var_BID_VAR_RFC = var_BID_VAR_RFC.replace('_',' ')
    #print "var_BID_VAR_RFC set"
    
    var_BID_VAR_BANK = sys.argv[14]
    if var_BID_VAR_BANK == 'NA':
        var_BID_VAR_BANK = ''
    var_BID_VAR_BANK = var_BID_VAR_BANK.replace('_',' ')
    #print "var_BID_VAR_BANK set"
    
    var_BID_VAR_ACLABEDEST = sys.argv[15]
    if var_BID_VAR_ACLABEDEST == 'NA':
        var_BID_VAR_ACLABEDEST = ''
    var_BID_VAR_ACLABEDEST = var_BID_VAR_ACLABEDEST.replace('_',' ')
    #print "var_BID_VAR_ACLABEDEST set"
    
    var_BID_VAR_ALIASACC = sys.argv[16]
    if var_BID_VAR_ALIASACC == 'NA':
        var_BID_VAR_ALIASACC = ''
    var_BID_VAR_ALIASACC = var_BID_VAR_ALIASACC.replace('_',' ')
    #print "var_BID_VAR_ALIASACC set"
    
    var_BID_VAR_MAXAMT = sys.argv[17]
    if var_BID_VAR_MAXAMT == 'NA':
        var_BID_VAR_MAXAMT = ''
    var_BID_VAR_MAXAMT = var_BID_VAR_MAXAMT.replace('_',' ')
    #print "var_BID_VAR_MAXAMT set"
    
    var_BID_VAR_CORRACC = sys.argv[18]
    if var_BID_VAR_CORRACC == 'NA':
        var_BID_VAR_CORRACC = ''
    var_BID_VAR_CORRACC = var_BID_VAR_CORRACC.replace('_',' ')
    #print "var_BID_VAR_CORRACC set"
    
    var_BID_VAR_CLABEACC = sys.argv[19]
    if var_BID_VAR_CLABEACC == 'NA':
        var_BID_VAR_CLABEACC = ''
    var_BID_VAR_CLABEACC = var_BID_VAR_CLABEACC.replace('_',' ')
    #print "var_BID_VAR_CLABEACC set"
    
    var_BID_VAR_ADDRESS = sys.argv[20]
    if var_BID_VAR_ADDRESS == 'NA':
        var_BID_VAR_ADDRESS = ''
    var_BID_VAR_ADDRESS = var_BID_VAR_ADDRESS.replace('_',' ')
    #print "var_BID_VAR_ADDRESS set"
    
    var_BID_VAR_COLONY = sys.argv[21]
    if var_BID_VAR_COLONY == 'NA':
        var_BID_VAR_COLONY = ''
    var_BID_VAR_COLONY = var_BID_VAR_COLONY.replace('_',' ')
    #print "var_BID_VAR_COLONY set"
    
    var_BID_VAR_POSTALCD = sys.argv[22]
    if var_BID_VAR_POSTALCD == 'NA':
        var_BID_VAR_POSTALCD = ''
    var_BID_VAR_POSTALCD = var_BID_VAR_POSTALCD.replace('_',' ')
    #print "var_BID_VAR_POSTALCD set"
    
    var_BID_VAR_CITY = sys.argv[23]
    if var_BID_VAR_CITY == 'NA':
        var_BID_VAR_CITY = ''
    var_BID_VAR_CITY = var_BID_VAR_CITY.replace('_',' ')
    #print "var_BID_VAR_CITY set"
    
    var_BID_VAR_STATE = sys.argv[24]
    if var_BID_VAR_STATE == 'NA':
        var_BID_VAR_STATE = ''
    var_BID_VAR_STATE = var_BID_VAR_STATE.replace('_',' ')
    #print "var_BID_VAR_STATE set"
    
    var_BID_VAR_COUNTRY = sys.argv[25]
    if var_BID_VAR_COUNTRY == 'NA':
        var_BID_VAR_COUNTRY = ''
    var_BID_VAR_COUNTRY = var_BID_VAR_COUNTRY.replace('_',' ')
    #print "var_BID_VAR_COUNTRY set"
    
    var_BID_VAR_CURP = sys.argv[26]
    if var_BID_VAR_CURP == 'NA':
        var_BID_VAR_CURP = ''
    var_BID_VAR_CURP = var_BID_VAR_CURP.replace('_',' ')
    #print "var_BID_VAR_CURP set"
    
    var_BID_VAR_EMAIL = sys.argv[27]
    if var_BID_VAR_EMAIL == 'NA':
        var_BID_VAR_EMAIL = ''
    var_BID_VAR_EMAIL = var_BID_VAR_EMAIL.replace('_',' ')
    #print "var_BID_VAR_EMAIL set"
    
    var_BID_VAR_USACITY = sys.argv[28]
    if var_BID_VAR_USACITY == 'NA':
        var_BID_VAR_USACITY = ''
    var_BID_VAR_USACITY = var_BID_VAR_USACITY.replace('_',' ')
    #print "var_BID_VAR_USACITY set"
    
    var_BID_VAR_OFFICIALID = sys.argv[29]
    if var_BID_VAR_OFFICIALID == 'NA':
        var_BID_VAR_OFFICIALID = ''
    var_BID_VAR_OFFICIALID = var_BID_VAR_OFFICIALID.replace('_',' ')
    #print "var_BID_VAR_OFFICIALID set"
    
    var_BID_VAR_OFFICIALIDNUM = sys.argv[30]
    if var_BID_VAR_OFFICIALIDNUM == 'NA':
        var_BID_VAR_OFFICIALIDNUM = ''
    var_BID_VAR_OFFICIALIDNUM = var_BID_VAR_OFFICIALIDNUM.replace('_',' ')
    #print "var_BID_VAR_OFFICIALIDNUM set"
    
    var_BID_VAR_MPHONE = sys.argv[31]
    if var_BID_VAR_MPHONE == 'NA':
        var_BID_VAR_MPHONE = ''
    var_BID_VAR_MPHONE = var_BID_VAR_MPHONE.replace('_',' ')
    #print "var_BID_VAR_MPHONE set"

    var_BID_VAR_PHONE = sys.argv[32]
    if var_BID_VAR_PHONE == 'NA':
        var_BID_VAR_PHONE = ''
    var_BID_VAR_PHONE = var_BID_VAR_PHONE.replace('_',' ')
    #print "var_BID_VAR_PHONE set"
        
    var_BID_VAR_ACTIVITY = sys.argv[33]
    if var_BID_VAR_ACTIVITY == 'NA':
        var_BID_VAR_ACTIVITY = ''
    var_BID_VAR_ACTIVITY = var_BID_VAR_ACTIVITY.replace('_',' ')
    #print "var_BID_VAR_ACTIVITY set"
    
    var_BID_VAR_MONAMT = sys.argv[34]
    if var_BID_VAR_MONAMT == 'NA':
        var_BID_VAR_MONAMT = ''
    var_BID_VAR_MONAMT = var_BID_VAR_MONAMT.replace('_',' ')
    #print "var_BID_VAR_MONAMT set"
    
    var_BID_VAR_PEPOEXP = sys.argv[35]
    if var_BID_VAR_PEPOEXP == 'NA':
        var_BID_VAR_PEPOEXP = ''
    var_BID_VAR_PEPOEXP = var_BID_VAR_PEPOEXP.replace('_',' ')
    #print "var_BID_VAR_PEPOEXP set"
    
    var_BID_VAR_PAREPEPEXP = sys.argv[36]
    if var_BID_VAR_PAREPEPEXP == 'NA':
        var_BID_VAR_PAREPEPEXP = ''
    var_BID_VAR_PAREPEPEXP = var_BID_VAR_PAREPEPEXP.replace('_',' ')
    #print "var_BID_VAR_PAREPEPEXP set"

    var_BID_VAR_NOMPEPEXP = sys.argv[37]
    if var_BID_VAR_NOMPEPEXP == 'NA':
        var_BID_VAR_NOMPEPEXP = ''
    var_BID_VAR_NOMPEPEXP = var_BID_VAR_NOMPEPEXP.replace('_',' ')
    #print "var_BID_VAR_NOMPEPEXP set"
    
    var_BID_VAR_PARE = sys.argv[38]
    if var_BID_VAR_PARE == 'NA':
        var_BID_VAR_PARE = ''
    var_BID_VAR_PARE = var_BID_VAR_PARE.replace('_',' ')
    #print "var_BID_VAR_PARE set"
    
#AVB END

    s = s.replace('BIDVARNOMCLIE', var_BID_VAR_NOMCLIE)
    s = s.replace('BIDVARAPEPAT', var_BID_VAR_APE_PAT)
    s = s.replace('BIDVARAPE_MAT', var_BID_VAR_APE_MAT)
    s = s.replace('BIDVARFNAC', var_BID_VAR_FNAC)
    if var_BID_VAR_GENDER == 'H':
        s = s.replace('BIDVARGENEROF','')
        s = s.replace('BIDVARGENEROM','X')
    else:
        s = s.replace('BIDVARGENEROF','X')
        s = s.replace('BIDVARGENEROM','')
    s = s.replace('BIDVARLNAC',var_BID_VAR_BIRTHPLACE)
#AVB BEGIN
    s = s.replace('BIDVARPNAC',var_BID_VAR_BIRTHCOUNTRY)
    s = s.replace('BIDVARNAC',var_BID_VAR_BIRTH)
    s = s.replace('BIDVARCURP',var_BID_VAR_CURP)
    s = s.replace('BIDVARRFC',var_BID_VAR_RFC)
    s = s.replace('BIDVARDOCU_IDEN',var_BID_VAR_OFFICIALID)
     #  s = s.replace('BID_VAR_FIEL',var_BID_VAR_)    """
    s = s.replace('BIDVARDOMI',var_BID_VAR_ADDRESS)
    s = s.replace('BIDVARDOL',var_BID_VAR_COLONY)
    s = s.replace('BIDVARMUNI',var_BID_VAR_CITY)
    s = s.replace('BIDVARESTADO',var_BID_VAR_STATE)  
    s = s.replace('BIDVARCP',var_BID_VAR_POSTALCD)
    s = s.replace('BIDVARPAIS',var_BID_VAR_COUNTRY)
    s = s.replace('BIDVARTELCASA',var_BID_VAR_PHONE)
    s = s.replace('BIDVARCEK',var_BID_VAR_MPHONE)
    s = s.replace('BIDVAREMAIL',var_BID_VAR_EMAIL)
    s = s.replace('BIDVARACTIVIDAD',var_BID_VAR_ACTIVITY)
    s = s.replace('BIDVARINGMENSBRT',var_BID_VAR_MONAMT)
    if var_BID_VAR_PEPOEXP == 'Y':
        s = s.replace('BIDVARPPESI','X')
        s = s.replace('BIDVARPPENO','')
    else:
        s = s.replace('BIDVARPPESI','X')
        s = s.replace('BIDVARPPENO','')
    if var_BID_VAR_PAREPEPEXP == 'Y':
        s = s.replace('BIDVARPAREPPESI','X')
        s = s.replace('BIDVARPAREPPENO','')
    else:
        s = s.replace('BIDVARPAREPPESI','X')
        s = s.replace('BIDVARPAREPPENO','')
    s = s.replace('BIDVARNOMBPPE',var_BID_VAR_NOMPEPEXP)
    s = s.replace('BIDVARPAREPPE',var_BID_VAR_PARE)    
#AVB END
    #print "Content replaced"
        
    f = open(location + '/content.xml', 'w')
    f.write(s)
    f.close()
    #print "content written"
        #zip content again
    shutil.make_archive(sys.argv[4], 'zip', location)
    #print "Zip made"
        #delete zip file extension
    os.rename(sys.argv[4] + '.zip', sys.argv[4])
    #print "Document renamed"
    shutil.rmtree(location)
    #print "Document removed"
        #convert document
    #print "Calling unoconv -f pdf "
    print sys.argv[4]
    call(["unoconv", "-f", "pdf", sys.argv[4]])
        #remove odt file
    os.remove(sys.argv[4])
except:
    print "Unexpected error converting the template file to PDF:", sys.exc_info()[0]
    raise
