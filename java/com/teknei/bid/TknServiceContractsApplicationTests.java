package com.teknei.bid;

import com.teknei.bid.controller.rest.AcceptanceController;
import com.teknei.bid.dto.AcceptancePerCustomerDTO;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.persistence.entities.BidClieCont;
import com.teknei.bid.persistence.repository.BidClieContRepository;
import com.teknei.bid.service.ThumbService;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TknServiceContractsApplicationTests {

    //@Autowired
    private BidClieContRepository bidClieContRepository;
    //@Autowired
    //private ContractOTTService ottService;
    //@Autowired
    //private AcceptanceController acceptanceController;
    private static final Logger log = LoggerFactory.getLogger(TknServiceContractsApplicationTests.class);
    //@Autowired
    private ThumbService thumbService;

    //@Test
    public void testReflection(){
        BidClieCont cont = bidClieContRepository.findByIdClie(265l);
        log.info("Cont: {}", cont);
        cont = modify(cont);
        log.info("Cont2: {}", cont);
    }
//redeploy AVB 09-04-2018
    private BidClieCont modify(BidClieCont source){
        Class<?> clazz = source.getClass();
        Class<?> clazzString = String.class;
        try{
            Field[] fields = clazz.getDeclaredFields();
            for(Field f : fields){
                Class<?> clazzType =  f.getType();
                if(clazzType.isAssignableFrom(clazzString)){
                    f.setAccessible(true);
                    String previousValue = (String) f.get(source);
                    if(previousValue == null){
                        previousValue = "NA";
                    }
                    previousValue = previousValue.toString().replace(" ","_");
                    f.set(source, previousValue);
                }
            }
        }catch (Exception e) {
            log.error("Error assigning reflection value: {}", e.getMessage());
        }
        return source;
    }

    //@Test
    public void testThumbs(){
        String uriSource = "/home/amaro/115imageFingerprint.jpeg";
        String uriTarget = "/home/amaro/Imágenes/115imageFingerprintThumb.jpeg";
        try {
            thumbService.generateThumb(uriSource, uriTarget);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void assertDummy() {
        assertNotNull("Hello world");
    }

    /*
    @Test
    public void assertAcceptance() {
        OperationIdDTO operationIdDTO = new OperationIdDTO();
        operationIdDTO.setOperationId(2l);
        ResponseEntity<List<AcceptancePerCustomerDTO>> responseEntity = acceptanceController.getAcceptancesPerCustomer(operationIdDTO);
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());

        List<AcceptancePerCustomerDTO> acceptancePerCustomerDTOList = responseEntity.getBody();
        log.info("Working with list: {}", acceptancePerCustomerDTOList);
        acceptancePerCustomerDTOList.forEach(c -> c.setAccepted(true));
        ResponseEntity<String> responseEntity1 = acceptanceController.updateAcceptancePerCustomer(acceptancePerCustomerDTOList);
        log.info("Response: {}", responseEntity1.getBody());
        assertEquals(200, responseEntity1.getStatusCodeValue());
    }
    */

    //@Test
    public void testContractGeneration() {
        //byte[] contract = ottService.generateContract("JORGE", "AMARO", "AACJ88203", "1122", "AV SIEMPRE VIVA 123", "30-01-2018", "amaro.coria@gmail.com", "44554433", "22334455", 123l);
        //assertNotNull(contract);
    }

    //@Test
    public void contextLoads() throws IOException, InterruptedException {
        String command = "/home/amaro/bin/generate_signed.sh /home/amaro/bin/tkaddhuellapdf.jar \"/home/amaro/bin/202.pdf\" \"/home/amaro/bin/202_signed.pdf\" /home/amaro/bin/202binaryFingerprint.bin /home/amaro/bin/202imageFingerprint.jpeg 1 250 400";
        String[] cmd = {"/home/amaro/bin/generate_signed.sh", "home/amaro/bin/tkaddhuellapdf.jar", "\"/home/amaro/bin/202.pdf\"", "\"/home/amaro/bin/202_signed.pdf\"", "/home/amaro/bin/202binaryFingerprint.bin", "/home/amaro/bin/202imageFingerprint.jpeg", "1", "250", "400"};
        String command1[] = {"bash", "-c", "/home/amaro/bin/generate_signed.sh"};
        //Process p = Runtime.getRuntime().exec(command1);

        String line = "/opt/JAVA_HOME/jdk1.8.0_151/bin/java -jar /home/amaro/bin/tkaddhuellapdf.jar \"/home/amaro/bin/202.pdf\" \"/home/amaro/bin/202_signed.pdf\" /home/amaro/bin/202binaryFingerprint.bin /home/amaro/bin/202imageFingerprint.jpeg 1 250 400";
        CommandLine cmdLine = CommandLine.parse(line);
        DefaultExecutor executor = new DefaultExecutor();
        int exitValue = executor.execute(cmdLine);
        //p.waitFor();
    }


}
